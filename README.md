# Hugo Static Web Site

This repository contains a guide to generate a Hugo Static Web Site, generate a Container image and deploy it to k8spin.cloud.

## How this repo was started?

This repository was created at [gitlab.com](gitlab.com), then using `hugo` the following commands were executed:

```bash
$ cd /tmp
$ git clone git@gitlab.com:k8spin-open/examples/hugo-static-web-site.git
Cloning into 'hugo-static-web-site'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
$ cd hugo-static-web-site
$ hugo new site quickstart
Congratulations! Your new Hugo site is created in /tmp/hugo-static-web-site/quickstart.

Just a few more steps and you are ready to go:

1. Download a theme into the same-named folder.
   Choose a theme from https://themes.gohugo.io/, or
   create your own with the "hugo new theme <THEMENAME>" command.
2. Perhaps you want to add some content. You can add single files
   with "hugo new <SECTIONNAME>/<FILENAME>.<FORMAT>".
3. Start the built-in live server via "hugo server".

Visit https://gohugo.io/ for quickstart guide and full documentation.
$ cd quickstart
$ git submodule add https://github.com/budparr/gohugo-theme-ananke.git themes/ananke
Cloning into '/tmp/hugo-static-web-site/quickstart/themes/ananke'...
remote: Enumerating objects: 1349, done.
remote: Total 1349 (delta 0), reused 0 (delta 0), pack-reused 1349
Receiving objects: 100% (1349/1349), 4.13 MiB | 2.41 MiB/s, done.
Resolving deltas: 100% (729/729), done.
$ hugo new posts/my-first-post.md
/tmp/hugo-static-web-site/quickstart/content/posts/my-first-post.md created
$ echo 'theme = "ananke"' >> config.toml
$ hugo serve -D

                   | EN
+------------------+----+
  Pages            | 10
  Paginator pages  |  0
  Non-page files   |  0
  Static files     |  3
  Processed images |  0
  Aliases          |  1
  Sitemaps         |  1
  Cleaned          |  0

Total in 10 ms
Watching for changes in /tmp/hugo-static-web-site/quickstart/{content,data,layouts,static,themes}
Watching for config changes in /tmp/hugo-static-web-site/quickstart/config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

Open your browser at [http://localhost:1313/](http://localhost:1313/). You should see:

![first-post](assets/first-post.png)

`Ctrl+C` to continue this guide.

## Build the container image

[Hugo](https://gohugo.io) creates a static web site when `hugo` command is executed. This means that has to be exposed though a web server. We choose [goStatic](https://github.com/PierreZ/goStatic) binary and container image as base to build this image.

```bash
$ docker images pierrezemb/gostatic
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
pierrezemb/gostatic   latest              ee9d57222001        5 months ago        6.7MB
```

Let's take a look to this project's [Dockerfile](Dockerfile):

```Dockerfile
FROM pierrezemb/gostatic

COPY quickstart/public /srv/http
```

As you can see, we copied the content generated under `quickstart/public` directory into our container image.

First create the rendered content:

```bash
$ pwd
/tmp/hugo-static-web-site/quickstart
$ hugo -D

                   | EN
+------------------+----+
  Pages            | 10
  Paginator pages  |  0
  Non-page files   |  0
  Static files     |  3
  Processed images |  0
  Aliases          |  1
  Sitemaps         |  1
  Cleaned          |  0

Total in 18 ms
```

Then you are ready to build this container image:

```bash
$ cd ..
$ docker build -t hugo-static-web-site:local .
Sending build context to Docker daemon  8.054MB
Step 1/2 : FROM pierrezemb/gostatic
 ---> ee9d57222001
Step 2/2 : COPY quickstart/public /srv/http
 ---> 04dc9894660e
Successfully built 04dc9894660e
Successfully tagged hugo-static-web-site:local
```

Then this image is ready to be runned as a new container:

```bash
$ docker run -d --name hugo -p 81:8043 hugo-static-web-site:local
7c47362013ff792d7f2efaca4882b936704f6ed90a869d81d75eaaf7ce477b0a
```

Open your browser at [http://localhost:81/](http://localhost:81/). You should see:

![first-post-docker](assets/first-post-container.png)

## Deploy it to K8Spin.cloud

Look at [.gitlab-ci.yml](.gitlab-ci.yml) to see how to deploy it on [K8Spin.cloud](https://k8spin.cloud) free tier.
Also, take a look to deployment files at [deploy](deploy) path.

### Pro tip:

Replace [quickstart/config.toml](quickstart/config.toml) `baseURL` attribute to point to your ingress hostname.

From:

```toml
baseURL = "http://example.org/"
languageCode = "en-us"
title = "My New Hugo Site"
theme = "ananke"
```

to:

```toml
baseURL = "https://hugo-static-web-site.angelbarrerasanchez.apps.k8spin.cloud/"
languageCode = "en-us"
title = "My New Hugo Site"
theme = "ananke"
```
